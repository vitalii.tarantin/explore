$(function() {
    let $toggle = $('.toggle');

    $toggle.on('click',function() {
        if ($toggle.hasClass('active')) {
            $('body').css("background", "#D7DEEB");
            $toggle.removeClass('active');
        } else {
            $toggle.addClass('active');
            $('body').css("background", "rgba(20, 17, 18, 0.81)");

        }
    });
});