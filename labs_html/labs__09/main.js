// Dialog pop-up
const popUpElement = document.getElementById("pop-up");

const openBtn = document.getElementById("open-btn");
// If modal open
document.body.classList.add("scroll-lock");

// If modal close
document.body.classList.remove("scroll-lock");
openBtn.addEventListener("click", () => {
    popUpElement.show();
});
// -------------------------------------------------------------------------
const modalElement = document.getElementById("modal");

const openBtnModal = document.getElementById("open-btn-modal");

openBtnModal.addEventListener("click", () => {
    modalElement.showModal();
});
// ----------------------------------------------------------------------

const modalElementOpen = document.getElementById("modal__open");

const openBtnOpen = document.getElementById("open-btn-open");

openBtnOpen.addEventListener("click", () => {
    modalElementOpen.toggleAttribute("open");

    openBtnOpen.innerHTML = modalElementOpen.hasAttribute("open")
        ? "Close dialog windows"
        : "Open dialog windows";
});
